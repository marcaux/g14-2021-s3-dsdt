## intro

### This repo is only online because of archival purposes

** It is not recommended to use this method anymore **

If you are still interested in it, please read on.

This applies to the 2021 and 2022 G14 G15 models and enables legacy suspend (S3) on those.

## 2021 G14

This device still suffers from serious suspend issues while using s0ix.

- machine gets pulled out of the deepest power state for unknown reasons
- machine gets pulled out of the deepest power state when un-/plugging AC
- sometimes the machine fails to resume and only a hard reset helps to restart

The first two issues will be resolved soon: https://gitlab.freedesktop.org/drm/amd/-/issues/2248

Those patches probably will find it's way into 6.2 and most likely get backported to 6.1.

In the meantime S3 is still more reliable but be aware that the DSDT tables are not strictly static. When you battery is gone or the machine behaves oddly, be sure to rebuild them while booting without them.

## 2022 G14

Thanks to the recent findings of a bug in the ASUS firmware the Linux Kernel is able to patch the shortcomings and suspend properly with the following patches:

- https://lore.kernel.org/lkml/20220909180509.638-1-mario.limonciello@amd.com/
- https://gitlab.com/asus-linux/fedora-kernel/-/blob/rog-5.19/0005-acpi-x86-s2idle-Add-a-quirk-for-ASUS-ROG-Zephyrus-G1.patch

Both are upstreamed and landed in 6.1.

Arch users should get those patches with the latest -g14 kernel in the asus-linux Arch repository (https://asus-linux.org/wiki/arch-guide/#repo).

Fedora users can add the copr of Luke Jones and install the patched kernel in the meantime from there (https://copr.fedorainfracloud.org/coprs/lukenukem/asus-kernel/).

## 2021 G15

As there is still a firmware bug that prevents the second SSD from suspending on s0ix / s2idle this patch is still usefull.

### important

You need to re-do those steps everytime you update or downgrade the BIOS!

When you update / downgrade the BIOS first boot without the acpi_override image, apply the script again and reboot.

For GRUB users the most easy way to achieve this is:

- hold shift while the BIOS logo shows update
- the GRUB menu appears
- press **e** to edit the current selected entry
- remove the part from **initrd** where **acpi_override** is written
- boot with **ctrl+x**
- apply this patch again
- reboot and done

## requirements

`acpica-tools pcre-tools`

## how to create modified DSDT for you 2021 / 2022 G14 / G15 variant

- `git clone https://gitlab.com/marcaux/g14-2021-s3-dsdt.git`
- `cd g14-2021-s3-dsdt`
- `./modify-dsdt.sh`

## get acpi_override to load via GRUB

### Arch:

- edit `/etc/default/grub`

```
# GRUB_CMDLINE_LINUX_DEFAULT should already exist, so you have to add mem_sleep_default=deep to your existing line
# ... stands for the content you already have there, don't add ... ;)
GRUB_CMDLINE_LINUX_DEFAULT="mem_sleep_default=deep ..."

# GRUB_EARLY_INITRD_LINUX_CUSTOM has to be added, as it probably does not exist, yet
GRUB_EARLY_INITRD_LINUX_CUSTOM="acpi_override"
```

- regenerate grub
```
# grub-mkconfig -o /boot/grub/grub.cfg
```

### Fedora:
- edit `/etc/default/grub`

```
# GRUB_CMDLINE_LINUX should already exist, so you have to **add** mem_sleep_default=deep to your existing line
# ... stands for the content you already have there, don't add ... ;)
GRUB_CMDLINE_LINUX="mem_sleep_default=deep ..."

# GRUB_EARLY_INITRD_LINUX_CUSTOM has to be added, as it probably does not exist, yet
# not sure if this is needed still, see next step
GRUB_EARLY_INITRD_LINUX_CUSTOM="acpi_override"
```

- regenerate grub
```
# grub2-mkconfig -o /etc/grub2.cfg
```

- set the grubenv variable so it gets added dynamically for each entry

```
# grub2-editenv /boot/grub2/grubenv set early_initrd=acpi_override
```

### Fedora Silverblue:
- edit `/etc/default/grub`

```
# GRUB_EARLY_INITRD_LINUX_CUSTOM has to be added, as it probably does not exist, yet
GRUB_EARLY_INITRD_LINUX_CUSTOM="../../acpi_override"
```

- run `rpm-ostree kargs --editor`
- add `mem_sleep_default=deep` to the end of the existing options line so it could look like this (your existing parameters might look different):

```
# Current kernel arguments are shown below, and can be directly edited.
# Empty or commented lines (starting with '#') will be ignored.
# Individual kernel arguments should be separated by spaces, and the order 
# is relevant.
# Also, please note that any changes to the 'ostree=' argument will not be 
# effective as they are usually regenerated when bootconfig changes.

rhgb quiet root=UUID=bla-bli-blu rootflags=subvol=root mem_sleep_default=deep
```

- regenerate grub
```
# grub2-mkconfig -o /etc/grub2.cfg
```

## get acpi_override to load via systemd-boot

- add a loader to `/boot/loader/entries/` or edit an existing one
- add `initrd /acpi_override` before your Linux ramfs (likely the last `initrd` line)
- add `mem_sleep_default=deep` to your `options` line

as an example it could look like this:

```
title   Arch Linux
linux   /vmlinuz-linux
initrd  /acpi_override
initrd  /initramfs-linux.img
options root="LABEL=arch_os" rw mem_sleep_default=deep
```

## verify after cold boot

```
$ cat /sys/power/mem_sleep
s2idle [deep]
```

## Uninstall

### GRUB

- edit `/etc/default/grub`
  - remove `mem_sleep_default=deep`
  - remove the line containing `GRUB_EARLY_INITRD_LINUX_CUSTOM`
- check where your `grubenv` file is and execute `grub2-editenv {path to grubenv} unset early_initrd`
  - on Fedora and Silverblue: `grub2-editenv /boot/grub2/grubenv unset early_initrd`
- rebuild GRUB
  - on Fedora and Silverblue: `grub2-mkconfig -o /etc/grub2.cfg`

### systemd-boot

- edit the bootloader config of your choice in `/boot/loader/entries/`
- remove `initrd /acpi_override` before your Linux ramfs (likely the last `initrd` line)
- remove `mem_sleep_default=deep` from your `options` line
